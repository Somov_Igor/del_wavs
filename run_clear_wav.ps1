﻿#измеряем свободное место на диске С в %
$ListDisk = Get-WmiObject -Class Win32_LogicalDisk -Filter 'DeviceId = "C:"'
$DiskFreeSpace = ($ListDisk.freespace/1GB).ToString('F2')
$DiskFreeSpacePercent = [Math]::Round(($ListDisk.freespace/$ListDisk.size) * 100, 2)
#создаем переменную, указываем сколько % нам нужно достич для старта, менять вместе с $percentWarning в начале цикла WHILE
$percentWarning = 10
Start-Transcript -Append C:\bat\run_clear_wav.log
#стартуем постоянный цикл с условием "свобоное место" < "указанного размера"
WHILE ($DiskFreeSpacePercent -lt $percentWarning){
    $percentWarning = 10
    #измеряем свободное место на диске С в %
    Write-Host 'Измеряю размер диска C:, нужно достичь' $percentWarning%
    $ListDisk = Get-WmiObject -Class Win32_LogicalDisk -Filter 'DeviceId = "C:"'
    $DiskFreeSpace = ($ListDisk.freespace/1GB).ToString('F2')
    $DiskFreeSpacePercent = [Math]::Round(($ListDisk.freespace/$ListDisk.size) * 100, 2)
    #создаем переменные $Path... - пути до wav и zip
    Write-Host 'Свободно на диске C:' $DiskFreeSpacePercent%
    $PathWav = "C:\IS3\wavs\VEXT\rec"
    $PathArchiv = "C:\IS3\Logs\Archive"
    #создаем переменные $OldDate... - вычисляем дату создания самого старого файла
    $OldDateWav = (Get-ChildItem -Path $PathWav  -Filter "*.wav" | Sort-Object -Property LastWriteTime | select -First 1).LastWriteTime.Date
    $OldDateArchiv = (Get-ChildItem -Path $PathArchiv  -Filter "*.zip" | Sort-Object -Property LastWriteTime | select -First 1).LastWriteTime.Date
    Write-Host 'Дата для удаления .wav - '$OldDateWav
    Write-Host 'Дата для удаления .zip - '$OldDateArchiv
        #стартуем условие, если в каталоге с wav есть файлы старше, чем прошедшие сутки, то находим wav файлы за самый старый день и удаляем их, заново сканим диск и возврат в цикл WHILE и так пока не останутся файлы только за прошлый день 
        if ($OldDateWav -lt (get-date).Date){
            Write-Host 'Подготавливаю файлы .wav для удаления'
            Get-ChildItem -Path $PathWav -Filter *.wav | where {$_.LastWriteTime.Date -eq $OldDateWav}
            Get-ChildItem -Path $PathWav -Filter *.wav | where {$_.LastWriteTime.Date -eq $OldDateWav} | Remove-Item -Force
            #перескан диска        
            $ListDisk = Get-WmiObject -Class Win32_LogicalDisk -Filter 'DeviceId = "C:"'
            $DiskFreeSpace = ($ListDisk.freespace/1GB).ToString('F2')
            $DiskFreeSpacePercent = [Math]::Round(($ListDisk.freespace/$ListDisk.size) * 100, 2)
            Write-Host 'Файлы удалены, свободно на дисе C:' $DiskFreeSpacePercent%
            $Bugs = $Error.Count
                #Еще условие, если есть заблокированные файлы, которые нельзя удалить
                if ($Bugs -gt 0){
                    Write-Host 'количество ошибок - '$Error.Count
                    #Выдергиваем имя файла из ошибки
                    $NamesErrors = $Error.TargetObject
                    Write-Host 'Файлы с ошибкой' $NamesErrors
                        #раскидываем каждый файл в отдельную обработку
                        foreach ($Name in $NamesErrors) {
                           #Находим Процесс и дескриптор, и пересобираем его в нужный нам вид
                           $uHandle = (C:\bat\handle\handle64.exe $Name) -Match '[:]' -Split '\s+',7
                           $oHandle = [PSCustomObject][Ordered]@{
                                      'proc'   = $uHandle[0]
                                      'pid'    = $uHandle[2]
                                      'cid'    = $uHandle[5].TrimEnd(':')
                                      'handle' = $uHandle[6]
      
                                    }
                            Write-Host 'Следующий файл к разблокировке' 
                            $oHandle
                            #разблокируем файл(ы)
                            Write-Host 'команда разблокировки: C:\bat\handle\handle64.exe -c'$($oHandle.cid)'-p'$($oHandle.pid)'-y'
                            C:\bat\handle\handle64.exe -c $($oHandle.cid) -p $($oHandle.pid) -y
                            #удаляем файл(ы)
                            Write-Host 'Удаляю разблокированный файл -' $oHandle.handle
                            $oHandle.handle | Remove-Item -Force 
                            #Сбрасываем ошибки
                            $Error.Clear()
                            }
                #Сбрасываем ошибки
                $Error.Clear()    
                }
            #Сканируем наличие файлов wav
            $ScanFilesWav = test-path -path "$PathWav\*"
                if ($ScanFilesWav -eq $false){
                    Write-Host 'каталог ' $PathWav ' пуст, завершение скрипта'
                    exit
                }
            }
        #Если wav удалено много и остался 1 день, а место на диске не достигло $percentWarning, то смотрим каталог archiv, находим файлы zip за самый старый день и удаляем их, заново сканим диск и возврат в цикл WHILE и так пока не останутся файлы только за прошлые 7 дней
        elseif ($DiskFreeSpacePercent -ge $percentWarning){
        Write-Host 'Условие выполнено!'
        exit
        }
        elseif ($OldDateArchiv -lt (get-date).Date.AddDays(-7)){
            Write-Host 'Удаления .wav не достаточно, Подготавливаю файлы .zip для удаления'
            Get-ChildItem -Path $PathArchiv -Filter *.zip | where {$_.LastWriteTime.Date -eq $OldDateArchiv}
            Get-ChildItem -Path $PathArchiv -Filter *.zip | where {$_.LastWriteTime.Date -eq $OldDateArchiv} | Remove-Item -Force
            #перескан диска         
            $ListDisk = Get-WmiObject -Class Win32_LogicalDisk -Filter 'DeviceId = "C:"'
            $DiskFreeSpace = ($ListDisk.freespace/1GB).ToString('F2')
            $DiskFreeSpacePercent = [Math]::Round(($ListDisk.freespace/$ListDisk.size) * 100, 2)
            Write-Host 'Файлы удалены, свободно на дисе C:' $DiskFreeSpacePercent%
            #Сканируем наличие файлов zip
            $ScanFilesArchive = test-path -path "$PathArchiv\*"
                if ($ScanFilesArchive -eq $false){
                    Write-Host 'каталог ' $PathArchiv ' пуст, завершение скрипта'
                    exit
                }
         }
         #Если ни одно уловие до конца не выполнено, т.е. удалив wav и zip место на диске не достигло значения $percentWarning, то останавливаем скрипт. Ту можно добавить какое либо оповещение.
         else {
         Write-Host 'Не возможно завершить программу, т.к. удалены все файлы по условию, но свободное место на диске не достигло' $percentWarning% #маркер, можно удалять
         exit}
         }
Copy-Item C:\bat\run_clear_wav.log C:\bat\Old_run_clear_wav.log -Force
Get-ChildItem -Path C:\bat\run_clear_wav.log | where {$_.LastWriteTime.Date -eq (get-date).Date.AddDays(-7)} | Remove-Item -Force
Stop-Transcript