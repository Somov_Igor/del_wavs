﻿$names = Get-content ".\SRV.txt"

foreach ($name in $names){
    Invoke-Command -ComputerName $name -ScriptBlock {
        $action = New-ScheduledTaskAction -Execute "PowerShell.exe" -Argument "C:\bat\run_clear_wav.ps1"
        $trigger = New-ScheduledTaskTrigger -Daily -At 0:00
        $User= "NT AUTHORITY\SYSTEM"
        $task = Register-ScheduledTask -TaskName "run_clear_wav" -Trigger $trigger -User $User -Action $action -RunLevel Highest
        #$task.Triggers.Repetition.Duration = "P1D" #Повтор в течение одного дня
        $task.Triggers.Repetition.Interval = "PT60M" #Повтор каждые 60 минут
        $task | Set-ScheduledTask
    }
    }